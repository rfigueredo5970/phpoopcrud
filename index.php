<?php 
	define("DS",DIRECTORY_SEPARATOR);
	define("ROOT", realpath(dirname(__FILE__)) . DS);
	define("URL","http://localhost/PHPOOP_CRUD/");

	require_once "Config/Autoload.php";
	require_once "Models/Estudiante.php";

	Config\Autoload::run();
	require_once "Views/template.php";
	$request = new Config\Request();
	Config\Enrutador::run($request);
