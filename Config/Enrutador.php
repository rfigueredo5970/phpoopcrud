<?php namespace Config;

 class Enrutador{

 	public static function run(Request $request){
 		$controlador_name = $request->getControlador() . "Controller";
 		$ruta = ROOT . "Controllers" . DS . $controlador_name . ".php";
 		$metodo = $request->getMetodo(); 
 		$argumento = $request->getArgumento();
 		if (is_readable($ruta)) {
 			require_once $ruta;
 			$mostrar = "Controllers\\" .  $controlador_name;
 			$controlador = new $mostrar;
 			if (!isset($argumento)) {
 				$datos = call_user_func(array($controlador,$metodo));
 			} else {
 				$datos = call_user_func_array(array($controlador,$metodo),$argumento);
 			}
 			
 		}

 		// Cargar Vista
 		
 		$ruta = ROOT . "Views" . DS . $request->getControlador() . DS . $request->getMetodo() . ".php";
 		if (is_readable($ruta)) {
 			require_once $ruta;
 		}else{
 			print "Error 404 - No se encontro la ruta " . $ruta;
 		}
 	}

  }
