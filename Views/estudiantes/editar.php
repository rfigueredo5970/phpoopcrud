<?php $secciones = $estudiantes->listarSecciones(); ?>

<div class="box-principal">
	<h3 class="titulo">Editar estudiante </h3>
	<div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title">Editar estudiante <?php echo $datos['nombre']; ?><hr></h3>
		</div>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3">
				<div class="panel panel-default">
					<div class="panel-body">
						<img src="<?php echo URL;?>Views/template/imagenes/avatars/<?php echo $datos['imagen']?>" alt="imagen <?php echo $datos['imagen']?>" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-9">
		<form action="" class="form-horizontal" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<label for="inputNombre" class="control-label">Nombre del estudiante</label>
				<input type="text" class="form-control" id="inputNombre" name="nombre" value="<?php echo $datos['nombre']; ?>" required>
			</div>
			<div class="form-group">
				<label for="inputEdad" class="control-label">Edad</label>
				<input type="number" name="edad" id="inputEdad" class="formcontrol" value="<?php echo $datos['edad']?>" required>
			</div>
			<div class="form-group">
				<label for="inputPromedio" class="control-label">Promedio</label>
				<input type="number" name="promedio" id="inputPromedio" class="formcontrol" value="<?php echo $datos['promedio']?>"  required>
			</div>
			<div class="form-group">
				<label for="selectIdSeccion" class="control-label">Sección (Sección Actual: "<?php echo $datos['nombre_seccion']?>" )</label>
				<select name="id_seccion" id="selectIdSeccion" class="form-control">
					<?php while($row = mysqli_fetch_array($secciones)) {?>
						<option value="<?php echo $row['id']; ?>"><?php echo $row['nombre']; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-success">Enviar</button>
				<button type="reset" class="btn btn-warning">Borrar</button>	
			</div>
		<input type="hidden" name="id" value="<?php echo $datos['id'];?>" required>

		</form>
		<br><br><br>
	</div>





</div>
