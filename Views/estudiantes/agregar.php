<div class="box-principal">
	<h3 class="titulo">Agregar Estudiantes</h3>
	<div class="panel panel-success">
		<div class="panel-heading">
			<h3 class="panel-title">Agregar un nuevo estudiante</h3>
		</div>
		<div class="panel-body">
			<div class="row col-md-1"></div>
			<div class="col-md-10">
				<form action="" class="form-horizontal" method="POST" enctype="multipart/form-data">
					<div class="form-group">
						<label for="inputNombre" class="control-label">Nombre del Estudiante</label>
						<input type="text" class="form-control" id="inputNombre" name="nombre" required>
					</div>
					<div class="form-group">
						<label for="inputEdad" class="control-label">Edad</label>
						<input type="number" class="form-control" id="inputEdad" name="edad" required>
					</div>
					<div class="form-group">
						<label for="inputPromedio" class="control-label">Promedio</label>
						<input type="number" class="form-control" id="inputPromedio" name="promedio" required>
					</div>
					<div class="form-group">
						<label for="selectIdSeccion" class="control-label">Seccion</label>
						<select class="form-control" id="selectIdSeccion" name="id_seccion">
							<?php while($row = mysqli_fetch_array($datos) ) { ?>
								<option value="<?php echo $row['id']; ?>"><?php echo $row['nombre'] ?></option>
							<?php }?>
						</select>
					</div>
					<div class="form-group">
						<label for="inputImagen" class="control-label">Imagen</label>
						<input type="file"  id="inputImagen" name="imagen">
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success">Registrar</button>
						<button type="reset" class="btn btn-warning">Borrar</button>
					</div>
				</form>
			</div>
			<div class="row col-md-1"></div>
		</div>
	</div>
</div>