<div class="box-principal">
	<h3 class="titulo">Editar Secciones </h3>
	<div class="panel panel-success">
		<div class="panel-heading"> 
			<h3 class="panel-title">Editar sección: <?php echo $datos['nombre']; ?> <hr></h3>
		</div>
		<div class="panel-body">
			<div class="row col-md-1"></div>
			<div class="col-md-10">
				<form action="" class="form-horizontal" method="POST">
					<div class="form-group">
						<label for="inputNombre" class="control-label">Nombre de la Sección</label>
						<input type="text" class="form-control" id="inputNombre" name="nombre" value="<?php echo $datos['nombre']; ?>" required>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-success">Enviar</button>
						<button type="reset" class="btn btn-warning">Borrar</button>
					</div>
				</form>
			</div>
			<div class="row col-md-1"></div>
		</div>
	</div>
</div>