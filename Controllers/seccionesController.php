<?php namespace Controllers;

	use Models\Seccion as Seccion;

	class seccionesController{
		private $seccion;

		public function __construct(){
			$this->seccion = new Seccion;
		}

		public function index(){
			$datos = $this->seccion->listar();
			return $datos;
		}

		public function editar($id){
			$this->seccion->set("id",$id);
			if ($_POST) {
				$this->seccion->set("nombre",$_POST['nombre']);
				$this->seccion->edit();
				header("Location:" . URL . "secciones");
				return true;
			} else {
				$datos = $this->seccion->view();
				return $datos;
			}
			
		}

		public function agregar(){
			if ($_POST) {
				$this->seccion->set('nombre',$_POST['nombre']);
				$this->seccion->add();
				
			} 
		}

		public function eliminar($id){
			$this->seccion->set("id",$id);
			$this->seccion->delete();
			header("Location:" . URL . "secciones");
			
		}
	}